# IEOR 242 HW 3 
# Moyu Li

#install.packages(c('rpart','rpart.plot'))
library(dplyr)
library(ggplot2)
library(caTools) # splits
library(rpart) # CART
library(rpart.plot) # CART plotting
library(caret) # cross validation
library(GGally)
library(caTools)
library(ROCR)
library(randomForest)
library(gbm)
library(MASS)

# CART for classification with letter data
# https://datascience.stackexchange.com/questions/6048/decision-tree-or-logistic-regression
# https://cran.r-project.org/web/packages/rpart/vignettes/longintro.pdf

####################################################################
####################################################################
# a.

Letters242 <- read.csv("Letters242.csv")
set.seed(623)
train.ids = sample(nrow(Letters242), 0.65*nrow(Letters242))
Letters242.train = Letters242[train.ids,]
Letters242.test = Letters242[-train.ids,]

#ggplot(letter)
ggscatmat(Letters242)

####################################################################
####################################################################
# b.
# baseline model
Letters242$isB = as.factor(Letters242$letter == "B")
table(Letters242.test$isB)
# accuracy of base line model
827/(827+264)

####################################################################
# logistic regression 
mod <- glm(isB ~. -letter, data = Letters242.train, family="binomial")
summary(mod)
predTest = predict(mod, newdata=Letters242.test, type="response")
summary(predTest)
table(Letters242.test$isB, predTest > 0.5)

# accuracy of logistic regression model
(799+229)/(799+229+28+35)

# ROC curves
rocr.log.pred <- prediction(predTest, Letters242.test$isB)
logPerformance <- performance(rocr.log.pred, "tpr", "fpr")
plot(logPerformance, colorize = TRUE)
abline(0, 1)
as.numeric(performance(rocr.log.pred, "auc")@y.values)
# reference :https://rocr.bioinf.mpi-sb.mpg.de/ROCR.pdf

####################################################################
# CART model
mod2 <- rpart(isB ~. -letter, data = Letters242.train, method="class", minbucket=5, cp = 0.001)
summary(mod2)
prp(mod2)

cpVals = data.frame(cp = seq(0, .04, by=.002))
train.cart <- train(isB ~. -letter,
                    data = Letters242.train,
                    method = "rpart",
                    tuneGrid = cpVals,
                    trControl = trainControl(method = "cv", number=5),
                    metric = "Accuracy")

# look at the cross validation results, stored as a data-frame
# https://machinelearningmastery.com/machine-learning-evaluation-metrics-in-r/
train.cart$results # please ignore kappa
train.cart

# plot the results
ggplot(train.cart$results, aes(x=cp, y=Accuracy)) + geom_point()
# We can increase the size of the points:
ggplot(train.cart$results, aes(x=cp, y=Accuracy)) + geom_point(size=3)
# We can change the default axis labels
ggplot(train.cart$results, aes(x=cp, y=Accuracy)) + geom_point(size=3) +
  xlab("Complexity Parameter (cp)") + geom_line()

# Extract the best model and make predictions
train.cart$bestTune
mod.best = train.cart$finalModel
pred.best = predict(mod.best, newdata=Letters242.test, type="class")
table(Letters242.test$isB, pred.best)
table(Letters242.test$isB)
# accuracy 
(218+297+253+254)/(298+264+258+271)
(804+218)/(827+264)

####################################################################
# Random Forest 
mod.rf <- randomForest(isB ~ . -letter, data = Letters242.train)
pred.rf <- predict(mod.rf, newdata = Letters242.test) 
table(Letters242.test$isB, pred.rf)
# accuracy
(818+240)/(818+240+9+24)


####################################################################
####################################################################
# c.
# baseline model 
table(Letters242.train$letter)
table(Letters242.test$letter)
# accuracy 
258/(258+271+264+298)

####################################################################
# LDA model
LdaModel <- lda(letter ~. -isB, data = Letters242.train, family="multinomial")
predTestLDA <- predict(LdaModel, newdata = Letters242.test) 
#reference https://stat.ethz.ch/R-manual/R-devel/library/MASS/html/predict.lda.html
pred <- predTestLDA$class
tab <- table(Letters242.test$letter, pred)
accuracy <- sum(diag(tab))/ sum(tab)

####################################################################
# CART model
mod.cart <- rpart(letter ~. -isB, data = Letters242.train, method="class", minbucket=5, cp = 0.001)
summary(mod.cart)

cpVals = data.frame(cp = seq(0, .04, by=.002))
train.cart <- train(letter ~. -isB,
                    data = Letters242.train,
                    method = "rpart",
                    tuneGrid = cpVals,
                    trControl = trainControl(method = "cv", number=5),
                    metric = "Accuracy")

# look at the cross validation results, stored as a data-frame
# https://machinelearningmastery.com/machine-learning-evaluation-metrics-in-r/
train.cart$results # please ignore kappa
train.cart

# Extract the best model and make predictions
train.cart$bestTune
mod.best = train.cart$finalModel
pred.best = predict(mod.best, newdata=Letters242.test, type="class")
table(Letters242.test$letter, pred.best)
table(Letters242.test$letter)
# accuracy 
(271+218+240+232)/(298+264+258+271)

####################################################################
# Random Forest model
mod.rf <- randomForest(letter ~ .-isB, data = Letters242.train)
pred.rf <- predict(mod.rf, newdata = Letters242.test) 
table(Letters242.test$letter, pred.rf)
table(Letters242.test$letter)
# accuracy
(296+253+252+261)/(298+264+258+271)

####################################################################
# Random Forest model with cross validation

# bagging can be done just by setting mtry = p = 17
# need to use model matrix because of how factor x variables are handled
# "randomForest" doesn't break categorical variables for us, it treat them as one numeric column.
# train.ctr.mm = as.data.frame(model.matrix(CTR ~ . + 0, data = train.ctr)) # +0 here is to remove dependent column
# set.seed(3432)
#mod.bag <- randomForest(x = train.ctr.mm, y = train.ctr$CTR, mtry = 16, nodesize = 5, ntree = 500)
#pred.bag <- predict(mod.bag, newdata = test.ctr.mm)

# syntax is very similar to CART training
# Warning: this took about 20 mins on my computer
# set.seed(99)
train.rf <- train(letter ~ .-isB,
                  data = Letters242.train,
                  method = "rf",
                  tuneGrid = data.frame(mtry=1:15),
                  trControl = trainControl(method="cv", number=5, verboseIter = TRUE),
                  metric = "Accuracy")
train.rf$results
train.rf
best.rf <- train.rf$finalModel
pred.best.rf <- predict(best.rf, newdata = Letters242.test)
ggplot(train.rf$results, aes(x = mtry, y = Accuracy)) + geom_point(size = 3) + 
  ylab("CV Accuracy") + theme_bw() + theme(axis.title=element_text(size=18), axis.text=element_text(size=18))
table(Letters242.test$letter, pred.best.rf)
# accuracy
(295+251+252+262)/(298+264+258+271)

####################################################################
# Boosting

# distribution is saying that this is a regression problem
# ntrees is the number of iterations (number of trees to fit)
# shrinkage is shrinkage parameter aka learning rate aka step-size
# interaction.depth is the depth of each individual tree

mod.boost <- gbm(letter ~ . -isB,
                 data = Letters242.train,
                 distribution = "multinomial",
                 n.trees = 22400,
                 interaction.depth = 10)

# NOTE: we need to specify number of trees to get a prediction for boosting
pred.boost <- predict(mod.boost, newdata = Letters242.test, n.trees=22400, type="response")
pred = apply(pred.boost, 1, which.max)
pred = factor(pred, levels = c(1,2,3,4), labels = c("A", "B", "P", "R"))
table(Letters242.test$letter,pred)
# accuracy
c <- table(Letters242.test$letter, pred)
diag(prop.table(c, 1))
sum(diag(prop.table(c)))




